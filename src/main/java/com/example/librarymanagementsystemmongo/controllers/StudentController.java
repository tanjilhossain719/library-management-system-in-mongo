package com.example.librarymanagementsystemmongo.controllers;

import com.example.librarymanagementsystemmongo.dto.StdDTO;
import com.example.librarymanagementsystemmongo.enums.Gender;
import com.example.librarymanagementsystemmongo.model.Student;
import com.example.librarymanagementsystemmongo.service.StdService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping(value = "/students")
@AllArgsConstructor
public class StudentController {
    private final StdService studentService;


    @PostMapping(value = "/save-student")
    public ResponseEntity<?> saveStudent(@RequestBody @Valid Student student) {
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.saveStudent(student));
    }


    @PutMapping(value = "/update-student/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable(value = "id") String id, @RequestBody @Valid StdDTO stdDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(studentService.updateStudent(id, stdDTO));
    }


    @GetMapping("/search-student")
    public ResponseEntity<?> searchStudent(@RequestParam(value = "id", defaultValue = "") String id,
                                           @RequestParam(value = "stdName", defaultValue = "") String stdName,
                                           @RequestParam(value = "email", defaultValue = "") String email,
                                           @RequestParam(value = "address", defaultValue = "") String address,
                                           @RequestParam(value = "gender", defaultValue = "") Gender gender,
                                           @RequestParam(value = "dateOfBirth", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate dateOfBirth,
                                           @RequestParam(value = "borrowBook") Optional<Boolean> borrowBook,
                                           @RequestParam(value = "deleteStudent") Optional<Boolean> deleteStudent,
                                           @RequestParam(value = "fine") Optional<Integer> fine,
                                           @RequestParam(value = "page", defaultValue = "0") int page,
                                           @RequestParam(value = "size", defaultValue = "10") int size) {

        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "stdName");
        return ResponseEntity.status(HttpStatus.OK).body(studentService.searchStd(id, stdName, email, address,
                gender, dateOfBirth, borrowBook, deleteStudent, fine, pageRequest));
    }

    @GetMapping(value = "/student-by-email")
    public ResponseEntity<?> studentByEmail(@RequestParam(value = "email") String email) {
        Student student = studentService.findStudentByEmail(email);
        return ResponseEntity.status(HttpStatus.OK).body(student);
    }

//    @GetMapping(value = "/find-who-borrowed-or-not")
//    public ResponseEntity<?> findWhoBorrowedOrNot( @RequestParam(value = "check") boolean check){
//        List<Student> record = studentService.checkWhoBorrow(check);
//        return ResponseEntity.status(HttpStatus.OK).body(record);
//    }



    @GetMapping(value = "/student-by-id/{id}")
    public ResponseEntity<?> studentById(@PathVariable(value = "id") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(studentService.findStdById(id));
    }


    @PutMapping(value = "/delete-by-id/{id}")
    public ResponseEntity<?> deleteById(@PathVariable(value = "id") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(studentService.updateDelete(id));
    }


}
