
package com.example.librarymanagementsystemmongo.controllers;

import com.example.librarymanagementsystemmongo.model.ChargeRequest;
import com.example.librarymanagementsystemmongo.service.StripeService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Refund;
import com.stripe.param.ChargeCreateParams;
import com.stripe.param.RefundCreateParams;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class StripeController {
    private final StripeService chargeProcessService;

    @PostMapping("/charge")
    public ResponseEntity<String> charge(@RequestBody ChargeRequest chargeRequest)
            throws StripeException {
        ChargeCreateParams params = new ChargeCreateParams.Builder()
                .setAmount(chargeRequest.getAmount())
                .setCurrency(chargeRequest.getCurrency())
                .setDescription("Example charge")
                .setSource(chargeRequest.getStripeToken())
                .build();
        Charge charge = Charge.create(params);
        System.out.println(charge.getId());
        return new ResponseEntity<>("Payment processed successfully", HttpStatus.OK);

    }

    @PostMapping("/refund")
    public ResponseEntity<String> refund(@RequestParam(value = "id", defaultValue = "") String id,
                                         @RequestParam(value = "amount", defaultValue = "") long amount) throws StripeException {
        RefundCreateParams params = new RefundCreateParams.Builder()
                .setCharge(id)
                .setAmount(amount)
                .build();
        Refund refund = Refund.create(params);
        return new ResponseEntity<>("Refund processed successfully", HttpStatus.OK);
    }


    @ExceptionHandler(StripeException.class)
    public String handleError(Model model, StripeException ex) {
        model.addAttribute("error", ex.getMessage());
        return "result";
    }

}

