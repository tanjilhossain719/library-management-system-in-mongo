package com.example.librarymanagementsystemmongo.controllers;

import com.example.librarymanagementsystemmongo.dto.BookDTO;
import com.example.librarymanagementsystemmongo.enums.BookGroup;
import com.example.librarymanagementsystemmongo.model.Book;
import com.example.librarymanagementsystemmongo.service.BookService;
import com.example.librarymanagementsystemmongo.service.QrCodeService;
import com.google.zxing.WriterException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping(value = "secured/books")
@AllArgsConstructor
public class BookController {

    private BookService bookService;
    private QrCodeService qrCodeService;

    @GetMapping("/")


    @PostMapping(value = "/save-book")
    public ResponseEntity<?> saveBook(@RequestBody @Valid Book book) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.saveBook(book));
    }

    //****************** Generating QR code of book details **************//
    @GetMapping(value = "/generate/qr-code/{id}")
    public void getQR(@PathVariable(value = "id") String id, HttpServletResponse response) throws WriterException, IOException {
        Book book = bookService.findBookById(id);
        qrCodeService.generateQrCodeInImage(book.toString(), response.getOutputStream());
    }

    //******************* Generating Bar Code of book details ****************//
    @GetMapping(value = "/generate/bar-code/{id}")
    public void getBarCode(@PathVariable(value = "id") String id, HttpServletResponse response) throws WriterException, IOException {
        Book book = bookService.findBookById(id);
        qrCodeService.generateBarCodeInImage(book.toString(), response.getOutputStream());
    }


    @PutMapping(value = "/update-book/{id}")
    public ResponseEntity<?> updateBook(@PathVariable(value = "id") String id, @RequestBody @Valid BookDTO bookDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.updateBook(id, bookDTO));
    }

    @GetMapping(value = "/book-by-id/{id}")
    public ResponseEntity<?> bookById(@PathVariable(value = "id") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.findBookById(id));
    }

    @GetMapping(value = "/book-by-name")
    public ResponseEntity<?> bookByName(@RequestParam(value = "bookName") String bookName) {

        Book book = bookService.findBookByBookName(bookName);
        return ResponseEntity.status(HttpStatus.OK).body(book);
    }


    @GetMapping("/search-book")
    public ResponseEntity<?> searchBook(@RequestParam(value = "id", defaultValue = "") String id,
                                        @RequestParam(value = "bookName", defaultValue = "") String bookName,
                                        @RequestParam(value = "writerName", defaultValue = "") String writerName,
                                        @RequestParam(value = "bookGroup", defaultValue = "") BookGroup bookGroup,
                                        @RequestParam(value = "quantity") Optional<Integer> quantity,
                                        @RequestParam(value = "deleteBook") Optional<Boolean> deleteBook,
                                        @RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "100") int size) {

        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "id");
        return ResponseEntity.status(HttpStatus.OK).body(bookService.searchBook(id, bookName, writerName, bookGroup, quantity, deleteBook, pageRequest));

    }

    @DeleteMapping(value = "/delete-by-id/{id}")
    public ResponseEntity<?> deleteById(@PathVariable(value = "id") String id) {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.updateDelete(id));
    }

//    @GetMapping(value = "/find-which-Borrowed-or-not")
//    public ResponseEntity<?> findWhoBorrowedOrNot(@RequestParam(value = "check") boolean check) {
//        List<Book> record = bookService.checkWhichBorrow(check);
//        return ResponseEntity.status(HttpStatus.OK).body(record);
//    }

}
