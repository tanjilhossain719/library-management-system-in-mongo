package com.example.librarymanagementsystemmongo.controllers;


import com.example.librarymanagementsystemmongo.dto.BorrowBookDTO;
import com.example.librarymanagementsystemmongo.model.BorrowBook;
import com.example.librarymanagementsystemmongo.service.BorrowBookService;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@RestController
@RequestMapping(value = "/borrowBooks")
@AllArgsConstructor
public class BorrowBookController {
    private final BorrowBookService borrowBookService;

    @PostMapping(value = "/save-borrow-record")
    public ResponseEntity<?> saveBorrowRecord(@RequestBody @Valid BorrowBook borrowBook) throws JRException {
        return ResponseEntity.status(HttpStatus.CREATED).body(borrowBookService.saveBorrowBook(borrowBook));


    }


    @PutMapping(value = "/update-borrow-book-after-return")
    public ResponseEntity<?> updateBorrowBookAfterReturn(@RequestBody @Valid BorrowBook returnBook) {
        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.updateBorrowBookAfterReturn(returnBook));
    }


    @GetMapping(value = "/borrow-book-by-id/{id}")
    public ResponseEntity<?> borrowBookById(@PathVariable(value = "id") String id) {

        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.findBorrowBookById(id));
    }

    @PutMapping(value = "/delete-by-id/{id}")
    public ResponseEntity<?> deleteById(@PathVariable(value = "id") String id) {

        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.deleteById(id));
    }


    @GetMapping("/search-borrow-record")
    public ResponseEntity<?> searchBorrowRecord(@RequestParam(value = "id", defaultValue = "") String id,
                                                @RequestParam(value = "stdId", defaultValue = "") String stdId,
                                                @RequestParam(value = "bookId", defaultValue = "") String bookId,
                                                @RequestParam(value = "borrowDate", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate borrowDate,
                                                @RequestParam(value = "returnDate", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate returnDate,
                                                //@RequestParam(value = "borrowDate", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate fromDate,
                                                //@RequestParam(value = "returnDate", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate toDate,
                                                @RequestParam(value = "page", defaultValue = "0") int page,
                                                @RequestParam(value = "size", defaultValue = "10") int size) {

        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "borrowDate");
        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.searchBorrowRecord(id, stdId, bookId, borrowDate,
                returnDate, pageRequest));
    }


    //******************Practice*********************//
    @GetMapping("/search-book-list-of-borrow")
    public ResponseEntity<?> bookList(@RequestParam(value = "borrowDate", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate fromDate,
                                      @RequestParam(value = "returnDate", defaultValue = "") @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate toDate) {
        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.bookList(fromDate, toDate));

    }

    @PutMapping(value = "/update-borrow-book/{id}")
    public ResponseEntity<?> updateBorrowBook(@PathVariable(value = "id") String id, @RequestBody @Valid BorrowBookDTO borrowBookDTO) {
        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.updateBorrowBook(id, borrowBookDTO));
    }

//    @GetMapping(value = "/check-By-Std-id")
//    public ResponseEntity<?> checkWhoBorrow( @RequestParam(value = "stdId") String stdId){
//        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.checkByStd(stdId));
//    }
//
//        @GetMapping(value = "/check-empty")
//    public ResponseEntity<?> checkEmpty(){
//        return ResponseEntity.status(HttpStatus.OK).body(borrowBookService.checkEmpty());
//    }


}
