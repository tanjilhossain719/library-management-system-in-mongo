package com.example.librarymanagementsystemmongo.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.pdf417.PDF417Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;

@AllArgsConstructor
@Service
public class QrCodeService {

    public void generateQrCodeInImage(String data, OutputStream outputStream) throws WriterException, IOException {
        BitMatrix bitMatrix = new QRCodeWriter().encode(data, BarcodeFormat.QR_CODE, 200, 200);
        MatrixToImageWriter.writeToStream(bitMatrix, "jpeg", outputStream);

    }

    public void generateBarCodeInImage(String data, OutputStream outputStream) throws WriterException, IOException {
        BitMatrix bitMatrix = new PDF417Writer().encode(data, BarcodeFormat.PDF_417, 500, 200);
        MatrixToImageWriter.writeToStream(bitMatrix, "jpeg", outputStream);
    }
}
