package com.example.librarymanagementsystemmongo.service;


import com.example.librarymanagementsystemmongo.dto.BookDTO;
import com.example.librarymanagementsystemmongo.enums.BookGroup;
import com.example.librarymanagementsystemmongo.exceptions.InvalidRequestException;
import com.example.librarymanagementsystemmongo.exceptions.ResourceAlreadyExistException;
import com.example.librarymanagementsystemmongo.exceptions.ResourceNotFoundException;
import com.example.librarymanagementsystemmongo.model.Book;
import com.example.librarymanagementsystemmongo.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static org.springframework.data.support.PageableExecutionUtils.getPage;


@AllArgsConstructor
@Service
public class BookService {
    private final BookRepository bookRepository;
    private final MongoTemplate mongoTemplate;

    //********************Secured Use********************//
    public Book saveBook(Book book) {
        return bookRepository.findBookByBookName(book.getBookName()).map(oldBook -> {
            if (!oldBook.isDeleteBook()) {
                throw new ResourceAlreadyExistException("Book already exist with name: " + oldBook.getBookName());
            }
            oldBook.setDeleteBook(false);
            return bookRepository.save(oldBook);
        }).orElseGet(() -> {
            return bookRepository.save(book);
        });

    }


    //************************Internal Use*********************//
    private Book getById(String id) {
        return bookRepository.findById(id).
                orElseThrow(() -> new ResourceNotFoundException("Book not found by Id: " + id));
    }


    //************************Internal Use********************//
    public Optional<Book> findOptionalBookByBookName(String bookName) {
        return bookRepository.findBookByBookName(bookName);
    }

    //************************Secured Use*********************//
    public Boolean updateDelete(String id) {
        return findOptionalBookById(id).map(oldBook -> {
            oldBook.setDeleteBook(true);
            return savebook1(oldBook);
        }).orElseThrow(() -> new ResourceNotFoundException("Book not found by Id: " + id));
    }

    //*************************Internal Use********************//
    private Boolean savebook1(Book oldBook) {
        bookRepository.save(oldBook);
        return true;
    }


    //************************Secured Use**********************//
    public Book findBookByBookName(String bookName) {

        return bookRepository.findBookByBookNameAndDeleteBookIsFalse(bookName)
                .orElseThrow(() -> new ResourceNotFoundException("Book not found by name : " + bookName));
    }

    //**************************Internal Use******************//
    public Optional<Book> findOptionalBookById(String id) {
        return bookRepository.findById(id);
    }


    //*****************************Secured Use****************************//
    public Book findBookById(String id) {

        return bookRepository.findBookByIdAndDeleteBookIsFalse(id)
                .orElseThrow(() -> new ResourceNotFoundException("Book not found by id: " + id));

    }

    //********************************Secured Use*****************************//
    public Book updateBook(String id, BookDTO bookDTO) {

        return bookRepository.findBookByIdAndDeleteBookIsFalse(id).map(oldBook -> {
            Optional<Book> byBookName = findOptionalBookByBookName(bookDTO.getBookName());
            if (byBookName.isPresent() && id.equals(byBookName.get().getId())) {
                throw new ResourceAlreadyExistException("Book already exist by name : " + bookDTO.getBookName());
            }

            oldBook.setBookName(bookDTO.getBookName());
            oldBook.setWriterName(bookDTO.getWriterName());
            oldBook.setBookGroup(bookDTO.getBookGroup());
            oldBook.setQuantity(bookDTO.getQuantity());
            return bookRepository.save(oldBook);
        }).orElseThrow(() -> new ResourceNotFoundException("Book not found by Id: " + id));
    }


    //***********************************Secured Use*********************************//
    public Page<Book> searchBook(String id, String bookName, String writerName, BookGroup bookGroup,
                                 Optional<Integer> quantity, Optional<Boolean> deleteBook, PageRequest pageRequest) {

        if (findOptionalBookById(id).isPresent() && findOptionalBookById(id).get().isDeleteBook()) {
            throw new InvalidRequestException("Book id: " + id + " is deleted");
        }

        Criteria criteria = new Criteria();

        if (!id.isEmpty()) {
            criteria.and("_id").is(new ObjectId(id));
        }


        if (!StringUtils.isEmpty(bookName)) {
            String regex = MongoRegexCreator.INSTANCE.toRegularExpression(bookName, MongoRegexCreator.MatchMode.CONTAINING);
            assert regex != null;
            criteria.and("bookName").regex(regex, "i");
        }

        if (!StringUtils.isEmpty(writerName)) {
            String regex = MongoRegexCreator.INSTANCE.toRegularExpression(writerName, MongoRegexCreator.MatchMode.CONTAINING);
            assert regex != null;
//            ***same as*** if(regex!=null){..........}
            criteria.and("writerName").regex(regex, "i");
        }

        if (bookGroup != null) {
            criteria.and("bookGroup").is(bookGroup);
        }

        if (quantity.isPresent() && quantity.get() > 0) {
            criteria.and("quantity").is(quantity.get());
        }

        deleteBook.ifPresent(
                aBoolean -> criteria.and("deleteBook").is(deleteBook.get()));

        Query query = new Query().with(pageRequest);
        query.addCriteria(criteria);

        Query countQuery = new Query();
        countQuery.addCriteria(criteria);

        List<Book> results = mongoTemplate.find(query, Book.class);

        return getPage(results, pageRequest, () -> mongoTemplate.count(countQuery, Book.class));

    }

    //*************** Required data customizing For QR Code Generate **************************//
    public String getQRdata(Book book){
        return String.valueOf(book.getId())+"\n"+ book.getBookName();
    }


//    public List<Book> checkWhichBorrow(boolean check) {
//
//        List<Book> allUniqueBooks = borrowBookRepository.findAllUniqueBooks();
//        if (check) {
//            return allUniqueBooks;
//        } else {
//            List<Long> collect = borrowBookRepository.findAllUniqueBooks().stream().map(Book::getId).collect(Collectors.toList());
//            return bookRepository.findAllByIdNotIn(collect);
//        }
//    }
//

//    private List<Book> getAllRecord() {
//        return bookRepository.findAll();
//    }

}
