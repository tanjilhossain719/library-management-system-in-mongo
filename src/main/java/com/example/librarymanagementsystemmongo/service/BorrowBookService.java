package com.example.librarymanagementsystemmongo.service;


import com.example.librarymanagementsystemmongo.dto.BorrowBookDTO;
import com.example.librarymanagementsystemmongo.exceptions.InvalidRequestException;
import com.example.librarymanagementsystemmongo.exceptions.ResourceNotFoundException;
import com.example.librarymanagementsystemmongo.model.Book;
import com.example.librarymanagementsystemmongo.model.BookDTO;
import com.example.librarymanagementsystemmongo.model.BorrowBook;
import com.example.librarymanagementsystemmongo.model.Student;
import com.example.librarymanagementsystemmongo.repository.BookRepository;
import com.example.librarymanagementsystemmongo.repository.BorrowBookRepository;
import com.example.librarymanagementsystemmongo.repository.StudentRepository;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.support.PageableExecutionUtils.getPage;
//import static org.springframework.data.repository.support.PageableExecutionUtils.getPage;


@AllArgsConstructor
@Service
public class BorrowBookService {
    private BorrowBookRepository borrowBookRepository;
    private StdService studentService;
    private final BookRepository bookRepository;
    private final StudentRepository studentRepository;

    private final MongoTemplate mongoTemplate;


    //*************************Secured Use***********************//
    public BorrowBook saveBorrowBook(BorrowBook borrowBook) throws JRException {

        String filePath = "/home/zaag/library-management-system-mongo with QR Code/src/main/resources/templates/BorrowBookMemo.jrxml";
        List<BookDTO> dataset = borrowBook.getBooks().stream().map(book -> {
            BookDTO bookDTO = new BookDTO();
            bookDTO.setBookID(book.getId());
            bookDTO.setBookName(book.getBookName());
            return bookDTO;
        }).collect(Collectors.toList());
        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(dataset);
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("BorrowBook",jrBeanCollectionDataSource);
        parameter.put("FirstName", borrowBook.getStudent().getStdName().getFirstName());
        parameter.put("LastName", borrowBook.getStudent().getStdName().getLastName());
        parameter.put("Id", borrowBook.getStudent().getId());
        parameter.put("mail", borrowBook.getStudent().getEmail());
        parameter.put("add",borrowBook.getStudent().getAddress());
        //String date = borrowBook.getBorrowDate().plusDays(3).toString();
        parameter.put("ReturnDate",LocalDate.now().plusDays(3).toString());
//        LocalDate temp = LocalDate.now().plusDays(3) ;
       // parameter.put("returnDate", LocalDate.now().plusDays(3));
       // parameter.put("BorrowBook", jrBeanCollectionDataSource);
       // parameter.put("bar", "Name: "+borrowBook.getStudent().getStdName().getFullName()+"ID: "+borrowBook.getStudent().getId());
        JasperReport jasperReport = JasperCompileManager.compileReport(filePath);
        //JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(borrowBook.getBooks());
        JasperPrint print = JasperFillManager.fillReport(jasperReport, parameter, new JREmptyDataSource());
        JasperExportManager.exportReportToPdfFile(print, "/home/zaag/library-management-system-mongo with QR Code/src/main/resources/static/demo.pdf");


        //Student student = borrowBook.getStudent();
        Student student = borrowBook.getStudent();

        if (studentService.findStdById(student.getId()) == null) {
            throw new ResourceNotFoundException("No student record is found by id " + student.getId());
        }

        student = studentService.findStdById(student.getId());

        if (student.isBorrowBookBoolean()) {
            throw new InvalidRequestException("The student id " + student.getId() + " already borrowed books");
        }

        if (borrowBook.getBooks().size() > 5) {
            throw new InvalidRequestException("A student can't borrowed more then 5 books");
        }


        if (!borrowBook.getBooks().isEmpty()) {
            borrowBook.getBooks().forEach(book -> {
                bookRepository.findBookByIdAndQuantityIsGreaterThan(book.getId(), 0).map(saveBook -> {
                    saveBook.setQuantity(saveBook.getQuantity() - 1);
                    return bookRepository.save(saveBook);

                }).orElseThrow(() -> new ResourceNotFoundException("No book record found by id " + book.getId()));
            });
        }

        //Updating Student Record
        student.setBorrowBookBoolean(true);

        studentRepository.save(student);

        borrowBook.setBorrowDate(LocalDate.now());
        return borrowBookRepository.save(borrowBook);

    }

    //************************Secured Use**********************//
    public Boolean updateBorrowBookAfterReturn(BorrowBook borrowBook) {
        return borrowBookRepository.findBorrowBooksByIdAndDeleteBorrowBookIsFalse(borrowBook.getId()).map(borrowBooks -> {

            Student student1 = borrowBooks.getStudent();

            if (studentService.findStdById(student1.getId()) == null) {
                throw new ResourceNotFoundException("No student record is found by id " + student1.getId());
            }

            student1 = studentService.findStdById(student1.getId());

            if (!student1.isBorrowBookBoolean() || borrowBook.getBooks().isEmpty()) {
                throw new InvalidRequestException("The student id " + student1.getId() + " do not borrow any book");
            }

            borrowBook.getBooks().forEach(book -> {
                bookRepository.findBookByIdAndQuantityIsGreaterThanEqual(book.getId(), 0).map(saveBook -> {
                    saveBook.setQuantity(saveBook.getQuantity() + 1);
                    return bookRepository.save(saveBook);

                }).orElseThrow(() -> new ResourceNotFoundException("No book record found by id " + book.getId()));
            });

            //Updating Student Record
            borrowBooks.setReturnDate(LocalDate.now());
            int days = borrowBooks.getReturnDate().compareTo(borrowBooks.getBorrowDate());
            if (days > 2) {
                days = days - 2;
                student1.setFine(student1.getFine() + (days * 10));
            }
            student1.setBorrowBookBoolean(false);

            studentRepository.save(student1);

            borrowBookRepository.save(borrowBooks);
            return true;
        }).orElseThrow(() -> new ResourceNotFoundException("No borrow record found by id: +" + borrowBook.getId()));

    }


    //************************Secured Use**************************//
    public BorrowBook updateBorrowBook(String id, BorrowBookDTO borrowBookDTO) {
        return borrowBookRepository.findBorrowBooksByIdAndDeleteBorrowBookIsFalse(id).map(borrowBook1 -> {
            borrowBook1.setStudent(borrowBookDTO.getStudent());
            borrowBook1.setBooks(borrowBookDTO.getBooks());
            borrowBook1.setBorrowDate(borrowBookDTO.getBorrowDate());
            borrowBook1.setReturnDate(borrowBookDTO.getReturnDate());
            return borrowBookRepository.save(borrowBook1);
        }).orElseThrow(() -> new ResourceNotFoundException("No borrow record found by id: +" + id));

    }

    //************************Internal Use**********************//
    public Optional<BorrowBook> findOptionalBorrowBookById(String id) {
        return borrowBookRepository.findById(id);
    }


    //*************************Secured Use***********************//
    public BorrowBook findBorrowBookById(String id) {
        return borrowBookRepository.findBorrowBooksByIdAndDeleteBorrowBookIsFalse(id)
                .orElseThrow(() -> new ResourceNotFoundException("No borrow record found by id: " + id));
    }

    //**************************Secured Use************************//
    public Boolean deleteById(String id) {
        return findOptionalBorrowBookById(id).map(borrowBook -> {
            Student student = borrowBook.getStudent();
            if (student.isBorrowBookBoolean()) {
                throw new InvalidRequestException(" Student id " + borrowBook.getStudent().getId() + " borrowed book. Can't delete without return");
            }
            borrowBook.setDeleteBorrowBook(true);
            borrowBookRepository.save(borrowBook);
            return true;
        }).orElseThrow(() -> new ResourceNotFoundException("No borrow record found by id " + id));
    }

    //**********************Secured Use************************//
    public Page<BorrowBook> searchBorrowRecord(String id, String stdId, String bookId, LocalDate borrowDate, LocalDate
            returnDate, PageRequest pageRequest) {


        Criteria criteria = new Criteria();

        if (!id.isEmpty()) {
            criteria.and("_id").is(new ObjectId(id));
        }

        if (!stdId.isEmpty()) {
            criteria.and("student._id").is(new ObjectId(stdId));
        }

        if (!bookId.isEmpty()) {
            criteria.and("books._id").is(new ObjectId(bookId));
        }

        if (borrowDate != null) {
            criteria.and("borrowDate").is(borrowDate);
        }

        if (returnDate != null) {
            criteria.and("returnDate").is(returnDate);
        }

//        if (fromDate != null && toDate != null) {
//              Criteria.where("fromDate").gte(fromDate).lt(toDate);
//            //criteria.and()
//        }
//
//        if(fromDate !=null && toDate !=null){
////            MatchOperation matchStage = Aggregation.match(new Criteria("foo").is("bar"));
//            @Aggregation(pipeline = {
//                    "{'$mat'}"
//            })
//        }

//        MatchOperation matchOperation = new MatchOperation(Criteria.where("borrowDate").gte(fromDate).lte(toDate));
//        ProjectionOperation projectionOperation = new ProjectionOperation().andExclude("books");
//        GroupOperation groupOperation = new GroupOperation("_id").sum("oid");


        Query query = new Query().with(pageRequest);
        query.addCriteria(criteria);

        Query countQuery = new Query();
        countQuery.addCriteria(criteria);


        List<BorrowBook> results = mongoTemplate.find(query, BorrowBook.class);

        return getPage(results, pageRequest, () -> mongoTemplate.count(countQuery, BorrowBook.class));

    }

    //****************No use just practice Aggregation**************************//
    public List<Map> bookList(LocalDate from, LocalDate to) {

        Aggregation aggregation = newAggregation(
                match(Criteria.where("borrowDate").gte(from).lte(to)),
                project("books")
                        .andExclude("_id").and("_id").as("id")
                        .and("books").size().as("size"),
                project("id", "size")
        );

        Document bookings = mongoTemplate.aggregate(aggregation, "borrowBookList", Map.class).getRawResults();
        List<Map> results = bookings.getList("results", Map.class);

        return results;

    }


//    public BorrowBook checkByStd(long id) {
//
//
//        return borrowBookRepository.findBorrowBooksByStudentId(id)
//                .orElseThrow(() -> new ResourceNotFoundExc("No borrow record found by id: " + id));
//
//    }
}
