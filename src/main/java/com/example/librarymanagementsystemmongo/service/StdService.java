package com.example.librarymanagementsystemmongo.service;

import com.example.librarymanagementsystemmongo.dto.StdDTO;
import com.example.librarymanagementsystemmongo.enums.Gender;
import com.example.librarymanagementsystemmongo.exceptions.InvalidRequestException;
import com.example.librarymanagementsystemmongo.exceptions.ResourceAlreadyExistException;
import com.example.librarymanagementsystemmongo.exceptions.ResourceNotFoundException;
import com.example.librarymanagementsystemmongo.model.Name;
import com.example.librarymanagementsystemmongo.model.Student;
import com.example.librarymanagementsystemmongo.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.support.PageableExecutionUtils.getPage;

@AllArgsConstructor
@Service
public class StdService {
    private final StudentRepository studentRepository;
    //   private final BorrowBookRepository borrowBookRepository;
    private final MongoTemplate mongoTemplate;


    //*************************Internal Use***********************//
//    public List<BorrowBook> searchForStudentOnly(String stdId) {
//        Specification<BorrowBook> matchStudentId = BorrowBookSpecification.matchStudentId(stdId);
//        Specification<BorrowBook> specification = Specification.where(matchStudentId);
//        return borrowBookRepository.findAll(specification);
//    }


    //**********************Secured Use (Controller Use)*************************//
    public Student saveStudent(Student student) {

        return studentRepository.findStdByEmail(student.getEmail()).map(oldStudent -> {
            if (!oldStudent.isDeleteStudent()) {
                throw new ResourceAlreadyExistException("Student already exist by email: " + student.getEmail());
            }
            oldStudent.setDeleteStudent(false);
            return studentRepository.save(oldStudent);

        }).orElseGet(() -> {
            Name name = student.getStdName();
            name.setFullName(name.getFirstName() + " " + name.getLastName());
            student.setStdName(name);
            return studentRepository.save(student);
        });


    }


    //***************************Internal Use************************//
    public Optional<Student> findOptionalStdByEmail(String email) {
        return studentRepository.findStdByEmail(email);
    }

    //****************************Internal Use**********************//
    public Optional<Student> findOptionalStdById(String id) {
        return studentRepository.findById(id);
    }

    //**************************Secured Use**********************//
    public Student findStdById(String id) {

        return studentRepository.findStudentByIdAndDeleteStudentIsFalse(id).
                orElseThrow(() -> new ResourceNotFoundException("Student not found by Id: " + id));

    }


    //Secured Use
    public Student findStudentByEmail(String email) {

        return studentRepository.findStudentByEmailAndDeleteStudentIsFalse(email)
                .orElseThrow(() -> new ResourceNotFoundException("Student not found by email: " + email));
    }

    //*************************Secured Use**********************//
    public Student updateStudent(String id, StdDTO stdDTO) {

        return studentRepository.findStudentByIdAndDeleteStudentIsFalse(id).map(oldStudent -> {

            Optional<Student> byEmail = findOptionalStdByEmail(stdDTO.getEmail());
            if (byEmail.isPresent() && id.equals(byEmail.get().getId())) {
                throw new ResourceAlreadyExistException("Student already exist by email: " + stdDTO.getEmail());
            }

            Name name = stdDTO.getStdName();
            name.setFullName(name.getFirstName() + " " + name.getLastName());
            stdDTO.setStdName(name);
            // oldStudent.
            oldStudent.setStdName(stdDTO.getStdName());
            oldStudent.setAddress(stdDTO.getAddress());
            oldStudent.setEmail(stdDTO.getEmail());
            oldStudent.setGender(stdDTO.getGender());
            oldStudent.setDateOfBirth(stdDTO.getDateOfBirth());
            return studentRepository.save(oldStudent);
        }).orElseThrow(() -> new ResourceNotFoundException("Student not found by Id: " + id));
    }

    private List<Student> getAllRecord() {
        return studentRepository.findAll();
    }


    //****************************Secured Use**************************//
    public Boolean updateDelete(String id) {
        return findOptionalStdById(id).map(oldStudent -> {
            oldStudent.setDeleteStudent(true);
            // return saveStudent1(oldStudent);
            studentRepository.save(oldStudent);
            return true;
        }).orElseThrow(() -> new ResourceNotFoundException("Student not found by Id: " + id));
    }

    //***************************Internal Use***********************//
    private Boolean saveStudent1(Student oldStudent) {
        studentRepository.save(oldStudent);
        return true;
    }

    //*********************Secured Use (Controller Use)**********************//
    public Page<Student> searchStd(String id, String stdName, String email, String address, Gender gender, LocalDate dateOfBirth,
                                   Optional<Boolean> borrowBook, Optional<Boolean> deleteStudent, Optional<Integer> fine,
                                   PageRequest pageRequest) {

        if (findOptionalStdById(id).isPresent() && findOptionalStdById(id).get().isDeleteStudent()) {
            throw new InvalidRequestException("Student id: " + id + " is not found");
        }

        Criteria criteria = new Criteria();

        if (!StringUtils.isEmpty(stdName)) {
            String regex = MongoRegexCreator.INSTANCE.toRegularExpression(stdName, MongoRegexCreator.MatchMode.CONTAINING);
            assert regex != null;
//            ***same as*** if(regex!=null){..........}
            criteria.and("stdName.fullName").regex(regex, "i");

        }

        if (!id.isEmpty()) {
            criteria.and("_id").is(new ObjectId(id));
        }

        if (!StringUtils.isEmpty(email)) {
            // criteria.and("email").is(email);
            String regex = MongoRegexCreator.INSTANCE.toRegularExpression(email, MongoRegexCreator.MatchMode.CONTAINING);
            assert regex != null;
            criteria.and("email").regex(regex, "i");
        }

        if (!StringUtils.isEmpty(address)) {
            String regex = MongoRegexCreator.INSTANCE.toRegularExpression(address, MongoRegexCreator.MatchMode.CONTAINING);
            assert regex != null;
            criteria.and("address").regex(regex, "i");
        }

        if (gender != null) {
            criteria.and("gender").is(gender);
        }

        if (dateOfBirth != null) {
            criteria.and("dateOfBirth").is(dateOfBirth);
        }

        borrowBook.ifPresent(
                aBoolean -> criteria.and("borrowBookBoolean").is(borrowBook.get()));

        deleteStudent.ifPresent(
                aBoolean -> criteria.and("deleteStudent").is(deleteStudent.get()));

        if (fine.isPresent() && fine.get() > 0) {
            criteria.and("fine").is(fine.get());
        }


        Query query = new Query().with(pageRequest);
        query.addCriteria(criteria);

        Query countQuery = new Query();
        countQuery.addCriteria(criteria);


        List<Student> results = mongoTemplate.find(query, Student.class);

        return getPage(results, pageRequest, () -> mongoTemplate.count(countQuery, Student.class));
    }


    //*********************Secured Use*******************//
//    public List<Student> checkWhoBorrow(boolean check) {
//        List<Long> allUniqueStudent = borrowBookRepository.findAllUniqueStudent();
//        if (check) {
//            return studentRepository.findAllByIdIn(allUniqueStudent);
//        } else {
//            return studentRepository.findAllByIdNotIn(allUniqueStudent);
//        }
//
//    }


}
