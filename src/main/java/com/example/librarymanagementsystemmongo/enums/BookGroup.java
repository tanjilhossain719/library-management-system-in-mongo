package com.example.librarymanagementsystemmongo.enums;

import java.io.Serializable;

public enum BookGroup implements Serializable {
    SCIENCE, COMMERCE, ARTS, OTHERS
}
