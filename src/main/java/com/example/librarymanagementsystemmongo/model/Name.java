package com.example.librarymanagementsystemmongo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Name implements Serializable {
    private static final long serialVersionUID = 789456L;

    @NotNull(message = "First Name must not be null")
    @Size(min = 2, max = 50, message = "First name length should be between {min} to {max}")
    private String firstName;

    @NotNull(message = "Last Name must not be null")
    @Size(min = 2, max = 50, message = "Last Name length should be between {min} to {max} ")
    private String lastName;
    private String fullName;

    public Name(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = this.firstName + this.lastName;
    }


}
