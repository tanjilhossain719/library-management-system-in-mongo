package com.example.librarymanagementsystemmongo.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class BookDTO implements Serializable {

    private static final long serialVersionUID = 54554541L;
    private String bookID;
    private String bookName;


}
