package com.example.librarymanagementsystemmongo.model;

import com.example.librarymanagementsystemmongo.enums.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
@Document(collection = "students")

public class Student implements Serializable {
    private static final long serialVersionUID = 41256987455211L;

    @Id
    private String id;

    @NotNull(message = "Email must not be null")
    //@Indexed(unique = true)
    @Email
    private String email;


    @NotNull(message = "Name must not be null")
    @Valid
    private Name stdName;


    @Size(min = 2, max = 200, message = "Address length must be between {min} to {max} ")
    private String address;

    private Gender gender;

    @NotNull(message = "Birthday date must not be null")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateOfBirth;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonIgnore
    private boolean borrowBookBoolean;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonIgnore
    private boolean deleteStudent;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonIgnore
    private int fine;

    // @EnableMongoAuditing
    //@PrePersist
    //@CreatedDate
//    public void prePersist() {
//        this.stdName.setFullName(this.stdName.getFirstName() + " " + this.stdName.getLastName());
//    }

    //@PreUpdate
//    @LastModifiedDate
//    public void preUpdate() {
//        this.stdName.setFullName(this.stdName.getFirstName() + " " + this.stdName.getLastName());
//    }

}
