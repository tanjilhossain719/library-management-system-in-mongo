package com.example.librarymanagementsystemmongo.model;

import com.example.librarymanagementsystemmongo.enums.BookGroup;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Document(collection = "books")

public class Book implements Serializable {
    private static final long serialVersionUID = 1230456L;

    @Id
    private String id;


    @NotNull(message = "Book name must not be null")
    @Size(min = 2, max = 50, message = "Book name length must be between {min} to {max}")
   // @Indexed(unique = true, name = "my")
    private String bookName;

    @NotNull(message = "Writer name must not be null")
    @Size(min = 2, max = 50, message = "Writer name length must be between {min} to {max}")
    private String writerName;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonIgnore
    private boolean deleteBook;

    @NotNull(message = "Group must not be null")
    private BookGroup bookGroup;

    @NotNull(message = "Quantity must not be null")
    private int quantity;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate bookEnterDate;


}
