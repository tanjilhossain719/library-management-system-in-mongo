package com.example.librarymanagementsystemmongo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


@NoArgsConstructor
@Getter
@Setter
@Document(collection = "borrowBookList")
public class BorrowBook implements Serializable {
    private static final long serialVersionUID = 8523146L;

    @Id
    private String id;

    //@ManyToOne
    //@DBRef
    @NotNull(message = "Student details must not be null")
    private Student student;

    // @ManyToMany
    //@DBRef
    @NotNull(message = "Book list must not be null")
    private List<Book> books;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonIgnore
    private boolean deleteBorrowBook;


    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate borrowDate;

    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDate returnDate;

    //@PrePersist
//    public void perPersist() {
//        this.borrowDate = LocalDate.now();
//    }


    @Override
    public String toString() {
        return "BorrowBook{" +
                "borrowDate=" + borrowDate +
                '}';
    }
}
