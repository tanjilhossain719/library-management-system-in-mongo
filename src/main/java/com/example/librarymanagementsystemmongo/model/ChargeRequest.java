package com.example.librarymanagementsystemmongo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChargeRequest {


//    private String id = UUID.randomUUID().toString();
    private String description;
    private long amount;
    private String currency;
    private String stripeEmail;
    private String stripeToken;
}
