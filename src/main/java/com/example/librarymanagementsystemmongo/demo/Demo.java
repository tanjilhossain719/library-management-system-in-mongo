//package com.example.librarymanagementsystemmongo.demo;
//
//import com.example.librarymanagementsystemmongo.exceptions.InvalidRequestException;
//
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.util.LinkedList;
//import java.util.List;
//
//public class Demo {
//    public static void main(String[] args) {
//        Booking first = new Booking();
//        LocalDateTime from = LocalDateTime.of(LocalDate.of(2022, 9, 1), LocalTime.of(8, 0, 0));
//        LocalDateTime to = LocalDateTime.of(LocalDate.of(2022, 9, 2), LocalTime.of(10, 0, 0));
//        first.setFrom(from);
//        first.setTo(to);
//
//        Booking second = new Booking();
//        from = LocalDateTime.of(LocalDate.of(2022, 9, 1), LocalTime.of(10, 1, 0));
//        to = LocalDateTime.of(LocalDate.of(2022, 9, 2), LocalTime.of(12, 1, 0));
//        second.setFrom(from);
//        second.setTo(to);
//
//        Booking third = new Booking();
//        from = LocalDateTime.of(LocalDate.of(2022, 9, 2), LocalTime.of(12, 2, 0));
//        to = LocalDateTime.of(LocalDate.of(2022, 9, 6), LocalTime.of(16, 0, 0));
//        third.setFrom(from);
//        third.setTo(to);
//
//        Booking forth = new Booking();
//        from = LocalDateTime.of(LocalDate.of(2022, 9, 7), LocalTime.of(8, 1, 0));
//        to = LocalDateTime.of(LocalDate.of(2022, 9, 10), LocalTime.of(12, 0, 0));
//        forth.setFrom(from);
//        forth.setTo(to);
//
//        Booking fifth = new Booking();
//        from = LocalDateTime.of(LocalDate.of(2022, 9, 8), LocalTime.of(13, 30, 0));
//        to = LocalDateTime.of(LocalDate.of(2022, 9, 9), LocalTime.of(18, 0, 0));
//        fifth.setFrom(from);
//        fifth.setTo(to);
//
//        Booking six = new Booking();
//        from = LocalDateTime.of(LocalDate.of(2022, 9, 10), LocalTime.of(14, 30, 0));
//        to = LocalDateTime.of(LocalDate.of(2022, 9, 11), LocalTime.of(15, 0, 0));
//        six.setFrom(from);
//        six.setTo(to);
//
//        Booking seven = new Booking();
//        from = LocalDateTime.of(LocalDate.of(2022, 9, 11), LocalTime.of(18, 0, 0));
//        to = LocalDateTime.of(LocalDate.of(2022, 9, 14), LocalTime.of(20, 0, 0));
//        seven.setFrom(from);
//        seven.setTo(to);
//
//        Booking temp = new Booking();
//        LocalDateTime fromTemp = LocalDateTime.of(LocalDate.of(2022, 9, 10), LocalTime.of(13, 1, 0));
//        LocalDateTime toTemp = LocalDateTime.of(LocalDate.of(2022, 9, 12), LocalTime.of(14, 0, 0));
//        temp.setFrom(fromTemp);
//        temp.setTo(toTemp);
//
//        List<Booking> list = new LinkedList<>();
//        list.add(first);
//        list.add(second);
//        list.add(third);
//        list.add(forth);
//        list.add(fifth);
//        list.add(six);
//        list.add(seven);
//
//        LocalDate fromDateTemp = fromTemp.toLocalDate();
//        LocalDate toTempDate = toTemp.toLocalDate();
//
//        LocalTime fromTempTime = fromTemp.toLocalTime();
//        LocalTime toTempTime = toTemp.toLocalTime();
//
//        for (Booking booking : list) {
//            LocalDate fromDate = booking.getFrom().toLocalDate();
//            LocalDate toDate = booking.getTo().toLocalDate();
//
//            LocalTime fromTime = booking.getFrom().toLocalTime();
//            LocalTime toTime = booking.getTo().toLocalTime();
//
////            if (((fromDateTemp.compareTo(fromDate) >= 0) && (fromDateTemp.compareTo(toDate) <= 0)) ||
////                    ((toTempDate.compareTo(fromDate) >= 0) && (toTempDate.compareTo(toDate) <= 0)) ||
////                    ((fromDateTemp.compareTo(fromDate) <= 0) && (toTempDate.compareTo(fromDate) >= 0)) ||
////                    ((fromDateTemp.compareTo(toDate) <= 0)) && ((toTempDate.compareTo(toDate) >= 0))) {
////
////                if (((fromTempTime.compareTo(fromTime) <= 0) && (toTempTime.compareTo(fromTime) >= 0)) ||
////                        ((fromTempTime.compareTo(toTime) <= 0) && (toTempTime.compareTo(toTime) >= 0)) ||
////                        ((fromTime.compareTo(fromTempTime) <= 0) && (toTime.compareTo(fromTempTime) >= 0)) ||
////                        ((fromTime.compareTo(toTempTime) <= 0) && (toTime.compareTo(toTempTime) >= 0))) {
////                    throw new InvalidRequestException("The time period is booked already. Try to book after " + toTime);
////                }
////
////            }
//
//
//            if (((fromDateTemp.compareTo(fromDate) >= 0) && (fromDateTemp.compareTo(toDate) <= 0)) ||
//                    ((toTempDate.compareTo(fromDate) >= 0) && (toTempDate.compareTo(toDate) <= 0)) ||
//                    ((fromDateTemp.compareTo(fromDate) <= 0) && (toTempDate.compareTo(toDate) >= 0))) {
//
//                if (((fromTempTime.compareTo(fromTime) >= 0) && (fromTempTime.compareTo(toTime) <= 0)) ||
//                        ((toTempTime.compareTo(fromTime) >= 0) && (toTempTime.compareTo(toTime) <= 0)) ||
//                        ((fromTempTime.compareTo(fromTime) <= 0) && (toTempTime.compareTo(toTime) >= 0))) {
//                    throw new InvalidRequestException("The time period is booked already. Try to book after " + toTime);
//                }
//
//            }
//
//            //((fromDateTemp.compareTo(fromDate) <= 0) && (toTempDate.compareTo(toDate) >= 0))
//
//        }
//
//        list.add(temp);
//
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i).getFrom()+" to "+ list.get(i).getTo());
//           // System.out.println(list.get(i).getFrom().ge);
//        }
//
//
//
//    }
//}
//
////            if ((toTempDate.compareTo(fromDate) >= 0) && (toTempDate.compareTo(toDate) <= 0)) {
////
////                if ((fromTempTime.compareTo(toTime) <= 0) && (toTempTime.compareTo(toTime) >= 0)) {
////                    throw new InvalidRequestException("The time period is booked already. Try to book after " + toTime);
////                }else if ((toTempTime.compareTo(fromTime) >= 0) && (toTempTime.compareTo(toTime) <= 0)){
////                    throw new InvalidRequestException("The time period is booked already. Try to book after " + toTime);
////                }
////            }
