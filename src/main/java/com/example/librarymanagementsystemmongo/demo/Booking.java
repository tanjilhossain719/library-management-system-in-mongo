package com.example.librarymanagementsystemmongo.demo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Booking {
   private LocalDateTime from;
   private LocalDateTime to;
}
