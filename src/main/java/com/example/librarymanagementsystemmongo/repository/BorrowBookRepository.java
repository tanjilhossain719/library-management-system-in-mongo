package com.example.librarymanagementsystemmongo.repository;

import com.example.librarymanagementsystemmongo.model.BorrowBook;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.Optional;


public interface BorrowBookRepository extends MongoRepository<BorrowBook, String> {

    public Optional<BorrowBook> findBorrowBooksByIdAndDeleteBorrowBookIsFalse(String id);

//    @Query("select distinct u.books from BorrowBook as u")
//    public List<Book> findAllUniqueBooks();
//
//    @Query("select distinct u.student.id from BorrowBook as u")
//    public List<String> findAllUniqueStudent();

    public Optional<BorrowBook> findBorrowBooksByStudentId(String id);


}
