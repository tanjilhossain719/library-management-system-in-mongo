package com.example.librarymanagementsystemmongo.repository;

import com.example.librarymanagementsystemmongo.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends MongoRepository<Book, String> {
    public Optional<Book> findBookByBookName(String bookName);

    public List<Book> findAllByIdNotIn(List<String> id);

    public Optional<Book> findBookByBookNameAndDeleteBookIsFalse(String bookName);

    public Optional<Book> findBookByIdAndDeleteBookIsFalse(String id);

    public Optional<Book> findBookByIdAndQuantityIsGreaterThan(String id, int quantity);

    public Optional<Book> findBookByIdAndQuantityIsGreaterThanEqual(String id, int quantity);
}
