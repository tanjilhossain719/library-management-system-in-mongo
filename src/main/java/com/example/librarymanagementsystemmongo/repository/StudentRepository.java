package com.example.librarymanagementsystemmongo.repository;

import com.example.librarymanagementsystemmongo.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository

public interface StudentRepository extends MongoRepository<Student, String> {
    public Optional<Student> findStdByEmail(String email);

    public Optional<Student> findById(String id);

    public Optional<Student> findStudentByIdAndDeleteStudentIsFalse(String id);

    public Optional<Student> findStudentByEmailAndDeleteStudentIsFalse(String email);

    public List<Student> findAllByIdIn(List<String > id);

    public List<Student> findAllByIdNotIn(List<String> longList);
}
