package com.example.librarymanagementsystemmongo.dto;

import com.example.librarymanagementsystemmongo.enums.Gender;
import com.example.librarymanagementsystemmongo.model.Name;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class StdDTO {

    @Email(message = "Must be a valid mail")
    private String email;

    @Valid
    //@NotNull(message = "Name must not be null")
    private Name stdName;

    private String address;

    private Gender gender;

    //@NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate dateOfBirth;


}
