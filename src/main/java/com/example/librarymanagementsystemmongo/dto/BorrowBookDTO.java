package com.example.librarymanagementsystemmongo.dto;
import com.example.librarymanagementsystemmongo.model.Book;
import com.example.librarymanagementsystemmongo.model.Student;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.LocalDate;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BorrowBookDTO {

    //@DBRef
    private Student student;

    //@DBRef
    private List<Book> books;


    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate borrowDate;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate returnDate;

}
