package com.example.librarymanagementsystemmongo.dto;

import com.example.librarymanagementsystemmongo.enums.BookGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BookDTO {

    private String bookName;

    private String writerName;

    private BookGroup bookGroup;

    private int quantity;

}
